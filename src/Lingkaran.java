public class Lingkaran {

    // attribute declaration
    public double jariJari;
    private double phi = 3.14;
    private double luas;
    private double tinggi;

    // method declaration
    public double getLuas() {
        luas = phi * jariJari * jariJari;
        return luas;
    }

    public double getTinggi() {
        tinggi = 2 * jariJari;
        return tinggi;
    }
}