public class mainObjek1 {

    public static void main(String[] args) throws Exception {
        System.out.println("================================================================");
        System.out.println("   Selamat Datang di Program Menghitung Luas dan Tinggi Robot");
        System.out.println("================================================================");

        Lingkaran objekLingkaranKepala = new Lingkaran();
        objekLingkaranKepala.jariJari = 7;
        // System.out.println("Luas objek lingkaran kepala = " +
        // objekLingkaranKepala.getLuas());

        persegiPanjang objekPersegiPanjangBadan = new persegiPanjang();
        objekPersegiPanjangBadan.panjang = 24;
        objekPersegiPanjangBadan.lebar = 15;
        // System.out.println("Luas objek persegi panjang badan = " +
        // objekPersegiPanjangBadan.getLuas());

        persegiPanjang objekPersegiPanjangTanganKiri = new persegiPanjang();
        objekPersegiPanjangTanganKiri.panjang = 18;
        objekPersegiPanjangTanganKiri.lebar = 4;
        // System.out.println("Luas objek persegi panjang tangan kiri = " +
        // objekPersegiPanjangTanganKiri.getLuas());

        persegiPanjang objekPersegiPanjangTanganKanan = new persegiPanjang();
        objekPersegiPanjangTanganKanan.panjang = 18;
        objekPersegiPanjangTanganKanan.lebar = 4;
        // System.out.println("Luas objek persegi panjang tangan kanan = " +
        // objekPersegiPanjangTanganKanan.getLuas());

        persegiPanjang objekPersegiPanjangKakiKiri = new persegiPanjang();
        objekPersegiPanjangKakiKiri.panjang = 8;
        objekPersegiPanjangKakiKiri.lebar = 15;
        // System.out.println("Luas objek persegi panjang kaki kiri = " +
        // objekPersegiPanjangKakiKiri.getLuas());

        persegiPanjang objekPersegiPanjangKakiKanan = new persegiPanjang();
        objekPersegiPanjangKakiKanan.panjang = 8;
        objekPersegiPanjangKakiKanan.lebar = 15;
        // System.out.println("Luas objek persegi panjang kaki kanan = " +
        // objekPersegiPanjangKakiKanan.getLuas());

        Lingkaran objekLingkaranTelapakTanganKiri = new Lingkaran();
        objekLingkaranTelapakTanganKiri.jariJari = 2;
        // System.out.println("Luas objek lingkaran telapak tangan kiri = " +
        // objekLingkaranTelapakTanganKiri.getLuas());

        Lingkaran objekLingkaranTelapakTanganKanan = new Lingkaran();
        objekLingkaranTelapakTanganKanan.jariJari = 2;
        // System.out.println("Luas objek lingkaran telapak tangan kanan = " +
        // objekLingkaranTelapakTanganKanan.getLuas());

        Lingkaran objekLingkaranTelapakKakiKiri = new Lingkaran();
        objekLingkaranTelapakKakiKiri.jariJari = 4;
        // System.out.println("Luas objek lingkaran telapak kaki kiri = " +
        // objekLingkaranTelapakKakiKiri.getLuas());

        Lingkaran objekLingkaranTelapakKakiKanan = new Lingkaran();
        objekLingkaranTelapakKakiKanan.jariJari = 4;
        // System.out.println("Luas objek lingkaran telapak kaki kanan = " +
        // objekLingkaranTelapakKakiKanan.getLuas());

        double luasTotalRobot = objekLingkaranKepala.getLuas() + objekPersegiPanjangBadan.getLuas()
                + objekPersegiPanjangTanganKiri.getLuas() + objekPersegiPanjangTanganKanan.getLuas()
                + objekLingkaranTelapakTanganKiri.getLuas() + objekLingkaranTelapakTanganKanan.getLuas()
                + objekPersegiPanjangKakiKiri.getLuas() + objekPersegiPanjangKakiKanan.getLuas()
                + objekLingkaranTelapakTanganKiri.getLuas() + objekLingkaranTelapakTanganKanan.getLuas()
                + objekLingkaranTelapakKakiKiri.getLuas() + objekLingkaranTelapakKakiKanan.getLuas();
        System.out.printf("Luas Total Robot = %1.2f", luasTotalRobot);

        double tinggiBadanRobot = objekLingkaranKepala.getTinggi() + objekPersegiPanjangBadan.lebar
                + objekPersegiPanjangKakiKanan.lebar + objekLingkaranTelapakKakiKiri.getTinggi();
        System.out.printf("\nTinggi Badan Robot = %1.2f", tinggiBadanRobot);
    }

}
