public class persegiPanjang {

    // attribute declaration
    public double panjang;
    public double lebar;
    private double luas;

    // method declaration
    public double getLuas() {
        // phi = 10
        // agar dia tetap memakai nilai phi variable global, maka jadinya seperti
        // dibawah ini
        // selain itu kita juga bebas memakai this kapanpun untuk memastikan bahwa yang
        // dipanggil adlah variable global.
        // luas = this.panjang * lebar;
        luas = panjang * lebar;
        return luas;
    }
}
